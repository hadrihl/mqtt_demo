CC	= gcc
CFLAGS	= -std=c99 -g -O3 -Wall #-Werror
LDFLAGS	= -lmosquitto

# Uncomment this to print out debugging info.
#CFLAGS += -DDEBUG

BIN	= client

all: $(BIN)

client: client.c
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	rm -rf *.o $(BIN)
